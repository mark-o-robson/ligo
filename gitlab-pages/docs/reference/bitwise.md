---
id: bitwise-reference
title: Bitwise
description: Operations on bytes
hide_table_of_contents: true
---

import Syntax from '@theme/Syntax';
import SyntaxTitle from '@theme/SyntaxTitle';

<SyntaxTitle syntax="pascaligo">
function and : nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="cameligo">
val and :  nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="reasonligo">
let and: (nat, nat) => nat
</SyntaxTitle>

A bitwise `and` operation.

<SyntaxTitle syntax="pascaligo">
function or : nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="cameligo">
val or :  nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="reasonligo">
let or: (nat, nat) => nat
</SyntaxTitle>

A bitwise `or` operation.

<SyntaxTitle syntax="pascaligo">
function xor : nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="cameligo">
val xor :  nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="reasonligo">
let xor: (nat, nat) => nat
</SyntaxTitle>

A bitwise `xor` operation.

<SyntaxTitle syntax="pascaligo">
function shift_left : nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="cameligo">
val shift_left :  nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="reasonligo">
let shift_left: (nat, nat) => nat
</SyntaxTitle>

A bitwise shift left operation.

<SyntaxTitle syntax="pascaligo">
function shift_right : nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="cameligo">
val shift_right :  nat -> nat -> nat
</SyntaxTitle>
<SyntaxTitle syntax="reasonligo">
let shift_right: (nat, nat) => nat
</SyntaxTitle>

A bitwise shift right operation.
