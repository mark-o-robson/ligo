export enum Language {
  PascaLigo = 'pascaligo',
  CameLigo = 'cameligo',
  ReasonLIGO = 'reasonligo'
}

export enum Command {
  Compile = 'compile',
  DryRun = 'dry-run',
  EvaluateValue = 'evaluate-value',
  EvaluateFunction = 'evaluate-function',
  Deploy = 'deploy',
  GenerateCommand = 'generate-command'
}

export enum Tool {
  TezosClient = 'tezos-client'
}

export enum ToolCommand {
  Originate = 'originate'
}
