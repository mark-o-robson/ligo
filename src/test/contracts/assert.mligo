let main (p, s : bool * unit) =
  let u : unit = assert p
  in ([] : operation list), s
